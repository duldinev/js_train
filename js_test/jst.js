var last = {
    x:0,
    y:0
  };
  //dim is the original image dimensions
  var dim = {
    width:500,
    height:427
  };
  var img = $("#holder img");
  img.bind('mousedown',checkpoint);
  
  function checkpoint(e){
    // ratio is the scaled image width/height
    // divided by the original width/height
    var ratio = {
      x:img.width()/dim.width,
      y:img.height()/dim.height
    };
  
    point = {
      x:e.clientX / ratio.x,
      y:e.clientY / ratio.y
    };
    var dis = distance(last.x,last.y,point.x,point.y);
    last=point;
    console.log(dis);
  }
  
  function distance(x1, y1, x2,y2){
      var dx = x2-x1;
      var dy = y2-y1;
      return Math.sqrt(dx*dx+dy*dy);
  }
  